#include <stdio.h>

#define NBL 19
#define NBC 19
//On considère que NBL et NBC sont des données pour quasiment toutes les fonctions/actions suivantes
//Ce sont des variables globales

typedef struct{
    //Définition de la structure Case
    //par une abscisse et une ordonnée (x et y) et le signe sur cette case
    int abscisse;
    int ordonnee;
    char signe;
} Case;

typedef struct{
    //Définition de la structure Plateau, une matrice de 19*19 cases
    Case mat[NBL][NBC];
} Plateau;

typedef struct{
    //Définition de la structure Score, deux entiers pour le nombre de prises
    int joueur_o;
    int joueur_x;
} Score;


/*********************************************************/
/* plateau_affichage                                     */
/* plateau : Donnee - Plateau                            */
/* i : Locale - Entier                                   */
/* j : Locale - Entier                                   */
/*********************************************************/

void plateau_affichage (Plateau* plateau){
	int i,j;

    /* Affichage des indices de colonnes */
    printf("     ");
    for (j=0; j<NBC; j++) printf("%2d   ",j+1);
    printf("\n");

    /* Affichage du plateau de jeu */
    for (i=0; i<NBL; i++) {
        printf("%2d   ",i+1);
        for (j=0; j<NBC; j++) printf("%2c   ", plateau->mat[i][j].signe); //chaque signe correspondant à la case
        printf("\n");
    }
    printf("\n");
}

/*********************************************************/
/* verification_horizontale                              */
/* plateau : Donnee - Plateau                            */
/* case_choisie : Donnee - Case                          */
/* signe_joueur : Donnee - Caractère                     */
/* fin_partie : Donnee/Resultat - Booléen                */
/* i : Locale - Entier                                   */
/* cpt : Locale - Entier                                 */
/*********************************************************/

void verification_horizontale(Plateau* plateau,Case case_choisie, char signe_joueur,int* fin_partie){
  int i; //compteur de décalage vers la gauche et la droite de la case choisie
  int cpt; //compteur du nombre d'apparitions du signe du joueur horizontalement de la case_choisie

  //l'ordonnée prend donc la valeur de i
  //on commence d'abord par compter le nombre de signes similaires à gauche
  //jusqu'à ce que l'on tombe sur autre chose (un . ou le signe adverse) ou que le compteur cpt soit =5
  cpt = 1;
  i = case_choisie.ordonnee - 1;
  while (cpt<5 && i>=0 && plateau->mat[case_choisie.abscisse][i].signe==signe_joueur ){
    cpt = cpt + 1;
    i = i - 1;
  }

  //puis on ajoute au compteur cpt le nombre de signes similaires à droite
  //jusqu'à ce que l'on tombe sur autre chose (un . ou le signe adverse) ou que le compteur cpt soit = 5
  i = case_choisie.ordonnee + 1;
  while (cpt<5 && i<=NBL && plateau->mat[case_choisie.abscisse][i].signe==signe_joueur ){ //droite
    cpt = cpt + 1;
    i = i + 1;
  }

  //si le compteur cpt a atteint 5 alors on déclare la fin de partie (avec un message correspondant au type d'alignement)
  if(cpt ==5) {
    *fin_partie = 1;
    printf("Joueur %c a gagné par 5 points alignés à l'horizontale\n\n", signe_joueur);
  }

}

/*********************************************************/
/* verification_verticale                                */
/* plateau : Donnee - Plateau                            */
/* case_choisie : Donnee - Case                          */
/* signe_joueur : Donnee - Caractère                     */
/* fin_partie : Donnee/Resultat - Booléen                */
/* i : Locale - Entier                                   */
/* cpt : Locale - Entier                                 */
/*********************************************************/

void verification_verticale(Plateau* plateau,Case case_choisie, char signe_joueur,int* fin_partie){
  int i; //compteur de décalage vers le haut et le bas de la case choisie
  int cpt; //compteur du nombre d'apparitions du signe du joueur verticalement de la case_choisie

  //l'abscisse prend donc la valeur de i
  //on commence d'abord par compter le nombre de signes similaires en haut
  //jusqu'à ce que l'on tombe sur autre chose (un . ou le signe adverse) ou que le compteur cpt soit =5
  cpt = 1;
  i = case_choisie.abscisse - 1;
  while (cpt<5 && i>=0 && plateau->mat[i][case_choisie.ordonnee].signe==signe_joueur ){ //haut
    cpt = cpt + 1;
    i = i - 1;
  }

  //puis on ajoute au compteur cpt le nombre de signes similaires en bas
  //jusqu'à ce que l'on tombe sur autre chose (un . ou le signe adverse) ou que le compteur cpt soit = 5
  i = case_choisie.abscisse + 1;
  while (cpt<5 && i<=NBL && plateau->mat[i][case_choisie.ordonnee].signe==signe_joueur){ //bas
    cpt = cpt + 1;
    i = i + 1;
  }

  //si le compteur cpt a atteint 5 alors on déclare la fin de partie (avec un message correspondant au type d'alignement)
  if(cpt ==5) {
    *fin_partie = 1;
    printf("Joueur %c a gagné par 5 points alignés à la verticale\n\n", signe_joueur);
  }

}

/*********************************************************/
/* verification_diagonale_gauche                         */
/* plateau : Donnee - Plateau                            */
/* case_choisie : Donnee - Case                          */
/* signe_joueur : Donnee - Caractère                     */
/* fin_partie : Donnee/Resultat - Booléen                */
/* i : Locale - Entier                                   */
/* j : Locale - Entier                                   */
/* cpt : Locale - Entier                                 */
/*********************************************************/

void verification_diagonale_gauche(Plateau* plateau,Case case_choisie, char signe_joueur,int* fin_partie){
  int i; //compteur de décalage vers le haut et le bas de la case choisie
  int j; //compteur de décalage vers la droite et la gauche de la case choisie
  int cpt; //compteur du nombre d'apparitions du signe du joueur diagonalement à gauche de la case_choisie

  //l'abscisse prend donc la valeur de i
  //l'ordonnée prend donc la valeur de j
  //on commence d'abord par compter le nombre de signes similaires en haut à gauche
  //jusqu'à ce que l'on tombe sur autre chose (un . ou le signe adverse) ou que le compteur cpt soit =5
  cpt = 1;
  i = case_choisie.abscisse - 1;
  j = case_choisie.ordonnee - 1;
  while (cpt<5 && i >= 0 && j >= 0 && plateau->mat[i][j].signe==signe_joueur){
    cpt = cpt + 1;
    i = i - 1;
    j = j - 1;
  }

  //puis on ajoute au compteur cpt le nombre de signes similaires en bas à droite
  //jusqu'à ce que l'on tombe sur autre chose (un . ou le signe adverse) ou que le compteur cpt soit = 5
  i = case_choisie.abscisse + 1;
  j = case_choisie.ordonnee + 1 ;
  while (cpt<5 && i <= NBL && j <= NBC && plateau->mat[i][j].signe==signe_joueur){
    cpt = cpt + 1;
    i = i + 1;
    j = j + 1;
  }

  //si le compteur cpt a atteint 5 alors on déclare la fin de partie (avec un message correspondant au type d'alignement)
  if(cpt ==5) {
    *fin_partie = 1;
    printf("Joueur %c a gagné par 5 points alignés à la diagonale\n\n", signe_joueur);
  }

}

/*********************************************************/
/* verification_diagonale_droite                         */
/* plateau : Donnee - Plateau                            */
/* case_choisie : Donnee - Case                          */
/* signe_joueur : Donnee - Caractère                     */
/* fin_partie : Donnee/Resultat - Booléen                */
/* i : Locale - Entier                                   */
/* j : Locale - Entier                                   */
/* cpt : Locale - Entier                                 */
/*********************************************************/

void verification_diagonale_droite(Plateau* plateau,Case case_choisie, char signe_joueur,int* fin_partie){
  int i; //compteur de décalage vers le haut et le bas de la case choisie
  int j; //compteur de décalage vers la droite et la gauche de la case choisie
  int cpt; //compteur du nombre d'apparitions du signe du joueur diagonalement à droite de la case_choisie

  //l'abscisse prend donc la valeur de i
  //l'ordonnée prend donc la valeur de j
  //on commence d'abord par compter le nombre de signes similaires en bas à gauche
  //jusqu'à ce que l'on tombe sur autre chose (un . ou le signe adverse) ou que le compteur cpt soit =5
  cpt = 1;
  i = case_choisie.abscisse + 1;
  j = case_choisie.ordonnee - 1;
  while (cpt<5 && i<=NBL && j>=0 && plateau->mat[i][j].signe==signe_joueur){
    cpt = cpt + 1;
    i = i + 1;
    j = j - 1;
  }

  //puis on ajoute au compteur cpt le nombre de signes similaires en haut à droite
  //jusqu'à ce que l'on tombe sur autre chose (un . ou le signe adverse) ou que le compteur cpt soit = 5
  i = case_choisie.abscisse - 1;
  j = case_choisie.ordonnee + 1 ;
  while (cpt<5 && i>=0 && j <= NBC && plateau->mat[i][j].signe==signe_joueur){
    cpt = cpt + 1;
    i = i - 1;
    j = j + 1;
  }
  //si le compteur cpt a atteint 5 alors on déclare la fin de partie (avec un message correspondant au type d'alignement)
  if(cpt ==5) {
    *fin_partie = 1;
    printf("Joueur %c a gagné par 5 points alignés à la diagonale\n\n", signe_joueur);
  }

}

/*********************************************************/
/* prise_gauche                                          */
/* plateau : Donnee/Resultat - Plateau                   */
/* case_choisie : Donnee - Case                          */
/* signe_joueur : Donnee - Caractère                     */
/* signe_adverse : Donnee - Caractère                    */
/* cpt_prise_o : Donnee/Resultat - Entier                */
/* cpt_prise_x : Donnee/Resultat - Entier                */
/*********************************************************/

void prise_gauche(Plateau* plateau, Case case_choisie, char signe_joueur, char signe_adverse, Score* score){
  //on vérifie si l'ordonnée de la case choisie est bien supérieure à 3
  //car si l'ordonnée est inférieure à 3, il ne peut pas y avoir de prise à gauche (et cela dépasserait les limites du plateau)
  if(case_choisie.ordonnee > 3){
    //on vérifie que juste à gauche de la case_choisie, on retrouve le signe adverse
    if(plateau->mat[case_choisie.abscisse][case_choisie.ordonnee-1].signe == signe_adverse){
      //on vérifie que deux cases à gauche de la case_choisie, on retrouve le signe adverse
      if(plateau->mat[case_choisie.abscisse][case_choisie.ordonnee-2].signe == signe_adverse){
        //on vérifie que trois cases à gauche de la case_choisie, on retrouve signe du joueur
        if(plateau->mat[case_choisie.abscisse][case_choisie.ordonnee-3].signe==signe_joueur){
          //les deux cases à gauche de la case_choisie prennent comme signe le '.' car la prise a lieu
          plateau->mat[case_choisie.abscisse][case_choisie.ordonnee-2].signe = '.';
          plateau->mat[case_choisie.abscisse][case_choisie.ordonnee-1].signe = '.';
          //on ajoute ensuite 1 prise au joueur concerné
          if(signe_joueur=='x'){
            score->joueur_x = score->joueur_x + 1;
          }
          else{
            score->joueur_o= score->joueur_o + 1;
          }
        }
      }
    }
  }
}

/*********************************************************/
/* prise_droite                                          */
/* plateau : Donnee/Resultat - Plateau                   */
/* case_choisie : Donnee - Case                          */
/* signe_joueur : Donnee - Caractère                     */
/* signe_adverse : Donnee - Caractère                    */
/* cpt_prise_o : Donnee/Resultat - Entier                */
/* cpt_prise_x : Donnee/Resultat - Entier                */
/*********************************************************/

void prise_droite(Plateau* plateau,Case case_choisie, char signe_joueur, char signe_adverse, Score* score){
  //on vérifie si l'ordonnée de la case choisie est bien inférieure au nombre de colonnes moins 3
  //car si l'ordonnée est supérieure à cela, il ne peut pas y avoir de prise à droite (et cela dépasserait les limites du plateau)
  if(case_choisie.ordonnee <= NBC-3){
    //on vérifie que juste à droite de la case_choisie, on retrouve le signe adverse
    if(plateau->mat[case_choisie.abscisse][case_choisie.ordonnee+1].signe == signe_adverse){
      //on vérifie que deux cases à droite de la case_choisie, on retrouve le signe adverse
      if(plateau->mat[case_choisie.abscisse][case_choisie.ordonnee+2].signe == signe_adverse){
        //on vérifie que trois cases à droite de la case_choisie, on retrouve le signe du joueur
        if(plateau->mat[case_choisie.abscisse][case_choisie.ordonnee+3].signe==signe_joueur){
          //les deux cases à droite de la case_choisie prennent comme signe le '.' car la prise a lieu
          plateau->mat[case_choisie.abscisse][case_choisie.ordonnee+2].signe = '.';
          plateau->mat[case_choisie.abscisse][case_choisie.ordonnee+1].signe = '.';
          //on ajoute ensuite 1 prise au joueur concerné
          if(signe_joueur=='x'){
            score->joueur_x= score->joueur_x + 1;
          }
          else{
            score->joueur_o= score->joueur_o + 1;
          }
        }
      }
    }
  }
}

/*********************************************************/
/* prise_bas                                             */
/* plateau : Donnee/Resultat - Plateau                   */
/* case_choisie : Donnee - Case                          */
/* signe_joueur : Donnee - Caractère                     */
/* signe_adverse : Donnee - Caractère                    */
/* cpt_prise_o : Donnee/Resultat - Entier                */
/* cpt_prise_x : Donnee/Resultat - Entier                */
/*********************************************************/

void prise_bas(Plateau* plateau,Case case_choisie, char signe_joueur, char signe_adverse, Score* score){
  //on vérifie si l'abscisse de la case choisie est bien inférieure au nombre de lignes moins 3
  //car si l'abscisse est supérieure à cela, il ne peut pas y avoir de prise en bas (et cela dépasserait les limites du plateau)
  if(case_choisie.abscisse <= NBL-3){
    //on vérifie que juste en bas de la case_choisie, on retrouve le signe adverse
    if(plateau->mat[case_choisie.abscisse+1][case_choisie.ordonnee].signe == signe_adverse){
      //on vérifie que deux cases en bas de la case_choisie, on retrouve le signe adverse
      if(plateau->mat[case_choisie.abscisse+2][case_choisie.ordonnee].signe == signe_adverse){
        //on vérifie que trois cases en bas de la case_choisie, on retrouve le signe du joueur
        if(plateau->mat[case_choisie.abscisse+3][case_choisie.ordonnee].signe ==signe_joueur){
          //les deux cases en bas de la case_choisie prennent comme signe le '.' car la prise a lieu
          plateau->mat[case_choisie.abscisse+2][case_choisie.ordonnee].signe = '.';
          plateau->mat[case_choisie.abscisse+1][case_choisie.ordonnee].signe = '.';
          //on ajoute ensuite 1 prise au joueur concerné
          if(signe_joueur=='x'){
            score->joueur_x = score->joueur_x + 1;
          }
          else{
            score->joueur_o= score->joueur_o + 1;
          }
        }
      }
    }
  }
}

/*********************************************************/
/* prise_haut                                            */
/* plateau : Donnee/Resultat - Plateau                   */
/* case_choisie : Donnee - Case                          */
/* signe_joueur : Donnee - Caractère                     */
/* signe_adverse : Donnee - Caractère                    */
/* cpt_prise_o : Donnee/Resultat - Entier                */
/* cpt_prise_x : Donnee/Resultat - Entier                */
/*********************************************************/
void prise_haut(Plateau* plateau,Case case_choisie, char signe_joueur, char signe_adverse, Score* score){
  //on vérifie si l'abscisse de la case choisie est bien supérieure à 3
  //car si l'abscisse est inférieure à cela, il ne peut pas y avoir de prise en haut (et cela dépasserait les limites du plateau)
  if(case_choisie.abscisse > 3){
    //on vérifie que juste en haut de la case_choisie, on retrouve le signe adverse
    if(plateau->mat[case_choisie.abscisse-1][case_choisie.ordonnee].signe == signe_adverse){
      //on vérifie que deux cases en haut de la case_choisie, on retrouve le signe adverse
      if(plateau->mat[case_choisie.abscisse-2][case_choisie.ordonnee].signe == signe_adverse){
        //on vérifie que trois cases en haut de la case_choisie, on retrouve le signe du joueur
        if(plateau->mat[case_choisie.abscisse-3][case_choisie.ordonnee].signe ==signe_joueur){
          //les deux cases en haut de la case_choisie prennent comme signe le '.' car la prise a lieu
          plateau->mat[case_choisie.abscisse-2][case_choisie.ordonnee].signe = '.';
          plateau->mat[case_choisie.abscisse-1][case_choisie.ordonnee].signe = '.';
          //on ajoute ensuite 1 prise au joueur concerné
          if(signe_joueur=='x'){
            score->joueur_x= score->joueur_x + 1;
          }
          else{
            score->joueur_o= score->joueur_o + 1;
          }
        }
      }
    }
  }
}

/*********************************************************/
/* prise_haut_gauche                                     */
/* plateau : Donnee/Resultat - Plateau                   */
/* case_choisie : Donnee - Case                          */
/* signe_joueur : Donnee - Caractère                     */
/* signe_adverse : Donnee - Caractère                    */
/* cpt_prise_o : Donnee/Resultat - Entier                */
/* cpt_prise_x : Donnee/Resultat - Entier                */
/*********************************************************/

void prise_haut_gauche(Plateau* plateau,Case case_choisie, char signe_joueur, char signe_adverse, Score* score){
  //on vérifie si l'abscisse et l'ordonnée de la case choisie sont bien supérieures à 3
  //sinon, il ne peut pas y avoir de prise en haut à gauche (et cela dépasserait les limites du plateau)
  if(case_choisie.abscisse > 3 && case_choisie.ordonnee > 3){
    //on vérifie que juste en haut à gauche de la case_choisie, on retrouve le signe adverse
    if(plateau->mat[case_choisie.abscisse-1][case_choisie.ordonnee-1].signe == signe_adverse){
      //on vérifie que deux cases en haut à gauche de la case_choisie, on retrouve le signe adverse
      if(plateau->mat[case_choisie.abscisse-2][case_choisie.ordonnee-2].signe == signe_adverse){
        //on vérifie que trois cases en haut à gauche de la case_choisie, on retrouve le signe du joueur
        if(plateau->mat[case_choisie.abscisse-3][case_choisie.ordonnee-3].signe ==signe_joueur){
          //les deux cases en haut à gauche de la case_choisie prennent comme signe le '.' car la prise a lieu
          plateau->mat[case_choisie.abscisse-2][case_choisie.ordonnee-2].signe = '.';
          plateau->mat[case_choisie.abscisse-1][case_choisie.ordonnee-1].signe = '.';
          //on ajoute ensuite 1 prise au joueur concerné
          if(signe_joueur=='x'){
            score->joueur_x = score->joueur_x + 1;
          }
          else{
            score->joueur_o= score->joueur_o + 1;
          }
        }
      }
    }
  }
}

/*********************************************************/
/* prise_bas_droite                                      */
/* plateau : Donnee/Resultat - Plateau                   */
/* case_choisie : Donnee - Case                          */
/* signe_joueur : Donnee - Caractère                     */
/* signe_adverse : Donnee - Caractère                    */
/* cpt_prise_o : Donnee/Resultat - Entier                */
/* cpt_prise_x : Donnee/Resultat - Entier                */
/*********************************************************/

void prise_bas_droite(Plateau* plateau,Case case_choisie, char signe_joueur, char signe_adverse, Score* score){
  //on vérifie si l'abscisse et l'ordonnée de la case choisie sont bien inférieures aux nombres de lignes et de colonnes moins 3
  //sinon, il ne peut pas y avoir de prise en bas à droite (et cela dépasserait les limites du plateau)
  if(case_choisie.abscisse <= NBL-3 && case_choisie.ordonnee <= NBC-3){
    //on vérifie que juste en bas à droite de la case_choisie, on retrouve le signe adverse
    if(plateau->mat[case_choisie.abscisse+1][case_choisie.ordonnee+1].signe == signe_adverse){
      //on vérifie que deux cases en bas à droite de la case_choisie, on retrouve le signe adverse
      if(plateau->mat[case_choisie.abscisse+2][case_choisie.ordonnee+2].signe == signe_adverse){
        //on vérifie que juste trois cases en bas à droite de la case_choisie, on retrouve le signe du joueur
        if(plateau->mat[case_choisie.abscisse+3][case_choisie.ordonnee+3].signe ==signe_joueur){
          //les deux cases en bas à droite de la case_choisie prennent comme signe le '.' car la prise a lieu
          plateau->mat[case_choisie.abscisse+2][case_choisie.ordonnee+2].signe = '.';
          plateau->mat[case_choisie.abscisse+1][case_choisie.ordonnee+1].signe = '.';
          //on ajoute ensuite 1 prise au joueur concerné
          if(signe_joueur=='x'){
            score->joueur_x = score->joueur_x + 1;
          }
          else{
            score->joueur_o= score->joueur_o + 1;
          }
        }
      }
    }
  }
}

/*********************************************************/
/* prise_haut_droite                                     */
/* plateau : Donnee/Resultat - Plateau                   */
/* case_choisie : Donnee - Case                          */
/* signe_joueur : Donnee - Caractère                     */
/* signe_adverse : Donnee - Caractère                    */
/* cpt_prise_o : Donnee/Resultat - Entier                */
/* cpt_prise_x : Donnee/Resultat - Entier                */
/*********************************************************/

void prise_haut_droite(Plateau* plateau,Case case_choisie, char signe_joueur, char signe_adverse, Score* score){
  //on vérifie si l'abscisse est supérieure à 3 et l'ordonnée bien inférieure au nombre de colonnes moins 3
  //sinon, il ne peut pas y avoir de prise en haut à droite (et cela dépasserait les limites du plateau)
  if(case_choisie.abscisse > 3 && case_choisie.ordonnee <= NBC-3){
    //on vérifie que juste en haut à droite de la case_choisie, on retrouve le signe adverse
    if(plateau->mat[case_choisie.abscisse-1][case_choisie.ordonnee+1].signe == signe_adverse){
      //on vérifie que juste deux cases en haut à droite de la case_choisie, on retrouve le signe adverse
      if(plateau->mat[case_choisie.abscisse-2][case_choisie.ordonnee+2].signe == signe_adverse){
        //on vérifie que trois cases en haut à droite de la case_choisie, on retrouve le signe joueur
        if(plateau->mat[case_choisie.abscisse-3][case_choisie.ordonnee+3].signe ==signe_joueur){
          //les deux cases en haut à droite de la case_choisie prennent comme signe le '.' car la prise a lieu
          plateau->mat[case_choisie.abscisse-2][case_choisie.ordonnee+2].signe = '.';
          plateau->mat[case_choisie.abscisse-1][case_choisie.ordonnee+1].signe = '.';
          //on ajoute ensuite 1 prise au joueur concerné
          if(signe_joueur=='x'){
            score->joueur_x= score->joueur_x+ 1;
          }
          else{
            score->joueur_o= score->joueur_o + 1;
          }
        }
      }
    }
  }
}

/*********************************************************/
/* prise_bas_gauche                                      */
/* plateau : Donnee/Resultat - Plateau                   */
/* case_choisie : Donnee - Case                          */
/* signe_joueur : Donnee - Caractère                     */
/* signe_adverse : Donnee - Caractère                    */
/* cpt_prise_o : Donnee/Resultat - Entier                */
/* cpt_prise_x : Donnee/Resultat - Entier                */
/*********************************************************/

void prise_bas_gauche(Plateau* plateau,Case case_choisie, char signe_joueur, char signe_adverse, Score* score){
  //on vérifie si l'ordonnée est supérieure à 3 et l'abscisse bien inférieure au nombre de lignes moins 3
  //sinon, il ne peut pas y avoir de prise en bas à gauche (et cela dépasserait les limites du plateau)
  if(case_choisie.abscisse <= NBL-3 && case_choisie.ordonnee > 3){
    //on vérifie que juste en bas à gauche de la case_choisie, on retrouve le signe adverse
    if(plateau->mat[case_choisie.abscisse+1][case_choisie.ordonnee-1].signe == signe_adverse){
      //on vérifie que deux cases en bas à gauche de la case_choisie, on retrouve le signe adverse
      if(plateau->mat[case_choisie.abscisse+2][case_choisie.ordonnee-2].signe == signe_adverse){
        //on vérifie que trois cases en bas à gauche de la case_choisie, on retrouve le signe du joueur
        if(plateau->mat[case_choisie.abscisse+3][case_choisie.ordonnee-3].signe ==signe_joueur){
          //les deux cases en bas à gauche de la case_choisie prennent comme signe le '.' car la prise a lieu
          plateau->mat[case_choisie.abscisse+2][case_choisie.ordonnee-2].signe = '.';
          plateau->mat[case_choisie.abscisse+1][case_choisie.ordonnee-1].signe = '.';
          //on ajoute ensuite 1 prise au joueur concerné
          if(signe_joueur=='x'){
            score->joueur_x = score->joueur_x+ 1;
          }
          else{
            score->joueur_o = score->joueur_o + 1;
          }
        }
      }
    }
  }
}

/*********************************************************/
/* joueur_joue                                           */
/* plateau : Donnee/Resultat - Plateau                   */
/* signe_joueur : Donnee - Caractère                     */
/* signe_adverse : Donnee - Caractère                    */
/* fin_partie : Donnee/Resultat - Booléen                */
/* cpt_prise_o : Donnee/Resultat - Entier                */
/* cpt_prise_x : Donnee/Resultat - Entier                */
/* case_choisie : Locale - Case                          */
/*********************************************************/

void joueur_joue (Plateau* plateau,char signe_joueur, char signe_adverse, int* fin_partie, Score* score){
  Case case_choisie;

  /* Demande les coordonnées au joueur */
    printf("Au tour de joueur %c\n", signe_joueur );
    printf("Saisir vos coordonnées de jeu : ");
    scanf("%d,%d", &case_choisie.abscisse,&case_choisie.ordonnee);
  //tant que ce n'est pas dans les dimensions du plateau il rejoue
  while((case_choisie.abscisse<0 || case_choisie.abscisse>NBL) ||(case_choisie.ordonnee<0 || case_choisie.ordonnee>NBC)){
    printf("Rejouez, les coordonnées doivent être comprises entre 1 et %d\n",NBC);
    printf("Saisir vos coordonnées de jeu : ");
    scanf("%d,%d", &case_choisie.abscisse,&case_choisie.ordonnee);
  };

  printf("\n" );
  //si la case_choisie est 0,0 alors le joueur abandonne et met fin à la partie
  if (case_choisie.abscisse==0 && case_choisie.ordonnee==0) {
    *fin_partie = 1;
    printf("Joueur %c gagne par abandon\n",signe_adverse);
  }
  else{
    //sinon si la case est un point, on peut bien mettre le nouveau signe de la case et ses coordonnées
    if(plateau->mat[case_choisie.abscisse-1][case_choisie.ordonnee-1].signe =='.'){
      plateau->mat[case_choisie.abscisse-1][case_choisie.ordonnee-1].signe = signe_joueur;
      case_choisie.abscisse = case_choisie.abscisse - 1;
      case_choisie.ordonnee = case_choisie.ordonnee - 1;
    }
    else{
      //et si la case n'est pas un point on fait rejouer le joueur sur une case vide
      printf("\nRejouez sur une case vide\n");
      joueur_joue(plateau,signe_joueur, signe_adverse, fin_partie, score);
    }
  }

  //on fait toutes les vérifications de victoire
  verification_horizontale(plateau,case_choisie,signe_joueur, fin_partie);
  verification_verticale(plateau,case_choisie,signe_joueur, fin_partie);
  verification_diagonale_gauche(plateau,case_choisie,signe_joueur, fin_partie);
  verification_diagonale_droite(plateau,case_choisie,signe_joueur, fin_partie);

  //on fait toutes les vérifications de prises
  prise_gauche(plateau,case_choisie,signe_joueur, signe_adverse, score);
  prise_droite(plateau,case_choisie,signe_joueur, signe_adverse, score);
  prise_haut(plateau,case_choisie,signe_joueur, signe_adverse, score);
  prise_bas(plateau,case_choisie,signe_joueur, signe_adverse, score);
  prise_haut_gauche(plateau,case_choisie,signe_joueur, signe_adverse, score);
  prise_haut_droite(plateau,case_choisie,signe_joueur, signe_adverse, score);
  prise_bas_gauche(plateau,case_choisie,signe_joueur, signe_adverse, score);
  prise_bas_droite(plateau,case_choisie,signe_joueur, signe_adverse, score);

  //on affiche à nouveau le plateau avec le nouveau pion et les prises effectués
  plateau_affichage(plateau);

  //on affiche le score de prises
  printf("\n                                       Joueur o  %d - %d Joueur x\n\n", score->joueur_o, score->joueur_x);

  //si un joueur a plus de 5 prises, on met fin à la partie
  if(score->joueur_o>= 5){
    printf("\n Le joueur o a gagné grâce à 5 prises\n");
    *fin_partie = 1;
  }
  if(score->joueur_x >= 5){
    printf("\nLe joueur x a gagné grâce à 5 prises\n");
    *fin_partie = 1;
  }

}

/*********************************************************/
/* main                                                  */
/* plateau : Locale - Plateau                            */
/* fin_partie : Locale - Booléen                         */
/* cpt_prise_o : Locale - Entier                         */
/* cpt_prise_x : Locale - Entier                         */
/*********************************************************/

int main(){
  Plateau plateau;
    int i,j;
    int fin_partie = 0;
  Score score;
    score.joueur_x = 0;
    score.joueur_o = 0;

    /* Remplissage du plateau par des cases vides '.' */
    for (i=0; i<NBL; i++) {
      for (j=0; j<NBC; j++) {
        plateau.mat[i][j].signe = '.';
      }
    }

    /* Affichage de toute le plateau */
    printf("\nLe plateau de jeu\n");
    plateau_affichage(&plateau);

    //tant que fin_partie est égale à 0 on peut continuer à jouer
    //si fin_partie est égale à 1 alors cela s'arrêtera à la fin du tour du joueur gagnant
    while (fin_partie==0) {
      //on fait jouer un joueur sur deux avec le signe o et l'autre avec le signe x
      if(i%2==0){
        joueur_joue(&plateau,'x','o', &fin_partie, &score);
      }
      else {
        joueur_joue(&plateau,'o','x',&fin_partie, &score);
      }
      i = i + 1;
    }

	return 0;
}
